#!/bin/bash

if [ -f .SQL_PASSWORD ]
then 
  export SQL_PASSWORD=`cat .SQL_PASSWORD`
else
  export SQL_PASSWORD=`openssl rand -base64 32`
  echo $SQL_PASSWORD > .SQL_PASSWORD
fi

echo "SQL_PASSWORD IS ${SQL_PASSWORD}"

docker-compose up --build
